# H. Wennlöf, 2018

from Keithley import KeithleySMU2400Series
import yaml
import time
import threading #For multithreading

from readoutThread import readoutThread

#########################

#Program needs to be run through IDLE to work properly with the plotting (on Windows anyway).
#Before functions like ramp_vsub are called, the data acquisition
#needs to be paused (as we can't have two processes writing to the
#Keithley at the same time). Pausing can be done by calling pauseAcq().
#Unpausing is done by calling resumeAcq().

startTime = time.time()
logFileName = "log_" + str(startTime) + ".log"

configuration_file = ''

with open('config_keithley_tj.yaml', 'r') as file:
    configuration_file = yaml.load(file)

vsub = KeithleySMU2400Series(configuration_file)
vsub.disable_output()

vsub.set_voltage(-1, unit='V')

vsub.enable_output()

#Returns current and error in current, in microamps
#print(vsub.get_current())

print("\n\nKeithley Initialised.\n")


#print(vsub.state())


vsub.ramp_vsub(-6, 'V')


#To pause the logging.
#need to do that before issuing other commands
#We can't do several Keithley operations at once.
def pauseAcq():
    t.pauseAcq()
    time.sleep(1)

def resumeAcq():
    t.restartAcq()

#Thread reads out, logs in file and plots
t = readoutThread(vsub, logFileName, startTime)

t.start()


#pauseAcq()
#vsub.ramp_vsub(0, 'V')
#resumeAcq()


